# Toy - terminal image viewer

## Overview
Toy is a tiny image viewer/ascii renderer for
terminal.  
It supports many functions including ascii mode,
colored ansi mode, various color modes (base16,
256 and grayscale), various character modes
(ascii, unicode halfblock or a custom unicode-enabled
palette), different resizing and dithering algorithms.  
You can define and save your own ascii palettes,
view images in gallery mode, convert them to
gz-compressed files for later viewing, print
most used colors and much more.

## Usage 
Simply run the program without any arguments to see
all available options.

Copy of the program's help output:
```
Usage of ./toy:
  -a    Disable preservation of the aspect ratio
  -b    Use bilinear interpolation sampling for 
        image resizing
  -c    Enable rendering with ANSI colors
  -d string
        Delete a palette from config
  -f float
        Adjust vertical resolution based on font
        height (default 0.5)
  -g    Instead of rendering, output a
        gzip-compressed file
  -h    Use unicode half-blocks for better resolution
  -i    Print additional image information
  -j int
        Number of CPU cores to use
  -k string
        Save a palette specified with -p
  -l string
        Dithering matrix to use. Available:
        FloydSteinberg, Burkes, Sierra
        (default "FloydSteinberg")
  -m string
        Color mode to use when rendering in ansi
        mode. Available:
        X11 (256 colors), X11GS (Grayscale 256),
        base16 (Base 16 colors) (default "X11")
  -o    Write current configuration to ~/.toy.json
  -p string
        Symbol palette to use
        (default " .,:;1f50$&#NM")
  -r    Reverse palette
  -s string
        Custom config file path to use
  -t    Output rendering time
  -u    Use dithering
  -x int
        Column dimensions
  -y int
        Row dimensions
```

To get the fanciest image look you may want to use:
`./toy -c -m X11 -h -b -u -l Sierra -t -i /path/to/image/file`

You may optionally add an `-o` switch to save currently
used configuration and avoid typing these every time.  
It will overwrite previously saved configuration.

If no resolution is specified (switches -x and -y),
it defaults to current terminal dimensions (linux only).

Toy supports specifying multiple files (either typing
them manually or just a shell wildcard like *), in
that case images will be rendered one after one in
sequence.

## Compatibility
Toy should be compileable (and working) on every
platform supported by golang compiler.

## Compilation
All you need is a go compiler of version 2+
(tested on 2.1.12).  
Simply run `go build toy.go` to compile it.  
You don't need any additional go packages.

## License
Licensed under AGPL-3.0-only.
