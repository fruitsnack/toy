//    Toy - terminal image viewer
//    Copyright (C) 2018  Fruitsnack

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License versin 3 as
//    published by the Free Software Foundation.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import "os"
import "fmt"
import "flag"
import "image"
import _ "image/png"
import _ "image/jpeg"
import "image/draw"
import "runtime"
import "sync"
import "time"
import "bytes"
import "syscall"
import "unsafe"
import "strconv"
import "encoding/json"
import "compress/gzip"
import "io"
import "reflect"

type SliceBoundary struct {
    MinX int
    MinY int
    MaxX int
    MaxY int
    Dx int
    Dy int
}

type winsize struct {
    Row uint16
    Col uint16
    Xpixel uint16
    Ypixel uint16
}

type rgb struct {
    R uint8
    G uint8
    B uint8
}

type DitherData struct {
    X int
    Y int
    Coeff int
    Divisor int
}

type Conf struct {
    Palettes map[string]string `json:"Palettes"`
    AspectRatio bool `json:"AspectRatio"`
    Cores int `json:"Cores"`
    ReversePalette bool `json:"ReversePalette"`
    AnsiColors bool `json:"AnsiColors"`
    FontHeight float64 `json:"FontHeight"`
    Bilinear bool `json:"Billinear"`
    RenderTime bool `json:"RenderTime"`
    ColorMode string `json:"ColorMode"`
    AddInfo bool `json:"AddInfo"`
    Dithering bool `json:"Dithering"`
    DitheringMatrix string `json:"DitheringMatrix"`
    HalfBlock bool `json:"HalfBlock"`
}

func CalculateSliceBoundaries (outcanvas *image.RGBA, index int, totalslices int) (*SliceBoundary) {
    var minx, miny, maxx, maxy, nx, ny int
    nx = outcanvas.Bounds().Dx()
    ny = outcanvas.Bounds().Dy()
    minx = 0
    maxx = nx
    miny = index*(ny/totalslices)
    if index == totalslices - 1 {
        maxy = ny
    } else {
        maxy = ((index+1)*(ny/totalslices))-1
    }
    Boundary := &SliceBoundary{minx, miny, maxx, maxy, nx, ny}
    return Boundary
}

func Bilinear(incanvas *image.RGBA, outcanvas *image.RGBA, wg *sync.WaitGroup, index int, totalslices int) {
    SliceBoundary := CalculateSliceBoundaries(outcanvas, index, totalslices)
    xratio := float64(incanvas.Bounds().Dx())/float64(SliceBoundary.Dx)
    yratio := float64(incanvas.Bounds().Dy())/float64(SliceBoundary.Dy)
    for i := SliceBoundary.MinY; i <= SliceBoundary.MaxY; i++ {
        for j := SliceBoundary.MinX; j <= SliceBoundary.MaxX; j++ {
            px := (int)(float64(j)*xratio)
            py := (int)(float64(i)*yratio)
            xdiff := float64(j)*xratio-float64(px)
            ydiff := float64(i)*yratio-float64(py)
            outind := i*outcanvas.Stride + j*4
            inind := py*incanvas.Stride + px*4
            if outind < SliceBoundary.Dy*SliceBoundary.Dx*4-4 && inind < incanvas.Bounds().Dx()*incanvas.Bounds().Dy()*4-incanvas.Stride-4 {
                R := uint8(float64(incanvas.Pix[inind])*(1-xdiff)*(1-ydiff) + float64(incanvas.Pix[inind+4])*(xdiff)*(1-ydiff) + float64(incanvas.Pix[inind+incanvas.Stride])*(ydiff)*(1-xdiff) + float64(incanvas.Pix[inind+incanvas.Stride+4])*(xdiff*ydiff))
                G := uint8(float64(incanvas.Pix[inind+1])*(1-xdiff)*(1-ydiff) + float64(incanvas.Pix[inind+4+1])*(xdiff)*(1-ydiff) + float64(incanvas.Pix[inind+incanvas.Stride+1])*(ydiff)*(1-xdiff) + float64(incanvas.Pix[inind+incanvas.Stride+4+1])*(xdiff*ydiff))
                B := uint8(float64(incanvas.Pix[inind+2])*(1-xdiff)*(1-ydiff) + float64(incanvas.Pix[inind+4+2])*(xdiff)*(1-ydiff) + float64(incanvas.Pix[inind+incanvas.Stride+2])*(ydiff)*(1-xdiff) + float64(incanvas.Pix[inind+incanvas.Stride+4+2])*(xdiff*ydiff))
                A := uint8(float64(incanvas.Pix[inind+3])*(1-xdiff)*(1-ydiff) + float64(incanvas.Pix[inind+4+3])*(xdiff)*(1-ydiff) + float64(incanvas.Pix[inind+incanvas.Stride+3])*(ydiff)*(1-xdiff) + float64(incanvas.Pix[inind+incanvas.Stride+4+3])*(xdiff*ydiff))
                outcanvas.Pix[outind] = R
                outcanvas.Pix[outind+1] = G
                outcanvas.Pix[outind+2] = B
                outcanvas.Pix[outind+3] = A
            }
        }
    }
    wg.Done()
}

func NearestNeighbor(incanvas *image.RGBA, outcanvas *image.RGBA, wg *sync.WaitGroup, index int, totalslices int) {
    SliceBoundary := CalculateSliceBoundaries(outcanvas, index, totalslices)
    xratio := (incanvas.Bounds().Dx()<<16)/SliceBoundary.Dx + 1
    yratio := (incanvas.Bounds().Dy()<<16)/SliceBoundary.Dy + 1
    for i := SliceBoundary.MinY; i <= SliceBoundary.MaxY; i++ {
        for j := SliceBoundary.MinX; j <= SliceBoundary.MaxX; j++ {
            px := (j*xratio)>>16
            py := (i*yratio)>>16
            outind := i*outcanvas.Stride + j*4
            inind := py*incanvas.Stride + px*4
            if outind < SliceBoundary.Dy*SliceBoundary.Dx*4 && inind < incanvas.Bounds().Dx()*incanvas.Bounds().Dy()*4 {
                outcanvas.Pix[outind] = incanvas.Pix[inind]
                outcanvas.Pix[outind+1] = incanvas.Pix[inind+1]
                outcanvas.Pix[outind+2] = incanvas.Pix[inind+2]
                outcanvas.Pix[outind+3] = incanvas.Pix[inind+3]
            }
        }
    }
    wg.Done()
}

func CalculateAspectRatio (img image.Image, tx int, ty int, preserveratio bool, FontHeight float64, halfblock bool, ansimode bool) (int, int) {
    var nx, ny int
    var inpratio float64
    if preserveratio {
        inpratio = (float64(img.Bounds().Dy())/float64(img.Bounds().Dx()))
        nx = tx
        ny = (int)(float64(tx)*inpratio)
    } else {
        nx = tx
        ny = ty
    }
    ny = (int)(float64(ny)*FontHeight)
    if halfblock && ansimode {
        ny = ny*2
    }
    return nx, ny
}

func ClampInt(in int, min int, max int) (int) {
    if in > max {
        return max
    } else if in < min {
        return min
    } else {
        return in
    }
}

func AbsInt (i int) (int) {
    var ret int
    if (i > 0) {
        ret = i
    } else {
        ret = -i
    }
    return ret
}

func MHDist (ar uint8, ag uint8, ab uint8, br uint8, bg uint8, bb uint8) (int) {
    return AbsInt(int(ar)-int(br))+AbsInt(int(ag)-int(bg))+AbsInt(int(ab)-int(bb))
}

func RGBAToX11 (ir uint8, ig uint8, ib uint8) (int, rgb) {

    lowest := 256
    index := 16
    tempindex := 16

    var cr uint8
    var col rgb
    for r := 0; r < 6; r++ {
        var cg uint8
        for g := 0; g < 6; g++ {
            var cb uint8
            for b := 0; b < 6; b++ {
                MD := MHDist(ir, ig, ib, cr, cg, cb)
                if (MD < lowest) {
                    lowest = MD
                    tempindex = index
                    col = rgb{cr,cg,cb}
                }
                index++
                if (b == 0) {cb += 95} else {cb += 40}
	    }
            if (g == 0) {cg += 95} else {cg += 40}
        }
        if (r == 0) {cr += 95} else {cr += 40}
    }
    return tempindex, col
}

func RGBAToX11GS (ir uint8, ig uint8, ib uint8) (int, rgb) {
    g := uint8(8)
    lowest := 255
    tempindex := 0
    var col rgb
    for i := 232; i< 256; i++ {
        MD := MHDist(ir, ig, ib, g, g, g)
        if (MD < lowest) {
            lowest = MD
            tempindex = i
            col = rgb{g,g,g}
        }
        g += 10
    }
    return tempindex, col
}

func RGBATo4bit (ir uint8, ig uint8, ib uint8, cols *[16]rgb) (int, rgb) {
    tempindex := 0
    lowest := 255
    var col rgb
    for k, v := range cols {
        MD := MHDist(ir, ig, ib, v.R, v.G, v.B)
        if (MD < lowest) {
             lowest = MD
             tempindex = k
             col = v
        }
    }
    return tempindex, col
}

func GenerateAddInfo (cc *map[int]int, colornum int, ansi bool) (string) {
    colors := string("\n")
    if len(*cc) < colornum {
        colornum = len(*cc)
    }
    for i := 0; i < colornum; i++ {
        var ind, largest int
        for k, v := range *cc {
            if v >= largest {
                largest = v
                ind = k
            }
        }
        delete(*cc, ind)
        if ansi {
            colors = colors + string("\033[48;5;") + strconv.Itoa(ind) + string(";m") + strconv.Itoa(largest) + string("\033[0m") + string("\033[0m") + string(" ")
        } else {
            colors = colors + string(rune(ind)) + string(":") + strconv.Itoa(largest) + string(" ")
        }
    }
    return colors
}

func ConvertColor (R uint8, G uint8, B uint8, colormode uint8, cols *[16]rgb) (int, rgb) {
    switch colormode {
        case 0:
            return RGBAToX11(R,G,B)
        case 1:
            return RGBATo4bit(R,G,B, cols)
        case 2:
            return RGBAToX11GS(R,G,B)
        default:
            return RGBAToX11(R,G,B)
    }
}

func ClampUint8 (a uint8, b int) uint8 {
    if (int(a) + b) > 255 {
        return 255
    } else if (int(a) + b) < 0 {
        return 0
    } else {
        return uint8(int(a) + b)
    }
}

func AddQuantError(incanvas *image.RGBA,index int, x int, y int, oldpixel rgb, newpixel rgb, coeff int, divisor int) () {

    qeR := (int(oldpixel.R) - int(newpixel.R))<<32
    qeG := (int(oldpixel.G) - int(newpixel.G))<<32
    qeB := (int(oldpixel.B) - int(newpixel.B))<<32

    a := (qeR/divisor * coeff)>>32
    b := (qeG/divisor * coeff)>>32
    c := (qeB/divisor * coeff)>>32

    ir := index + incanvas.Stride * y + x
    ig := index + incanvas.Stride * y + x + 1
    ib := index + incanvas.Stride * y + x + 2

    incanvas.Pix[ir] = ClampUint8(incanvas.Pix[ir], a)
    incanvas.Pix[ig] = ClampUint8(incanvas.Pix[ig], b)
    incanvas.Pix[ib] = ClampUint8(incanvas.Pix[ib], c)
}

func Dither (incanvas *image.RGBA, colormode uint8, matrix *[]DitherData) {
    cols := InitCols()
    for i := 2; i < incanvas.Bounds().Dy() - 2; i++ {
        for j := 0; j < incanvas.Bounds().Dx() - 2; j++ {

            index := i*incanvas.Stride + j*4

            R := incanvas.Pix[index]
            G := incanvas.Pix[index+1]
            B := incanvas.Pix[index+2]

            oldpixel := rgb{R,G,B}
            _, newpixel := ConvertColor(R,G,B, colormode, cols)

            incanvas.Pix[index] = newpixel.R
            incanvas.Pix[index + 1] = newpixel.G
            incanvas.Pix[index + 2] = newpixel.B

            for _,v := range *matrix {
                AddQuantError(incanvas, index, v.X, v.Y, oldpixel, newpixel, v.Coeff, v.Divisor)
            }
        }
    }
}

func AnsiConvert (incanvas *image.RGBA, buffer *bytes.Buffer, colormode uint8, addinfo bool, colornum int) {
    cols := InitCols()
    cc := make(map[int]int)
    for i := 0; i < incanvas.Bounds().Dy(); i ++ {
        for j := 0; j < incanvas.Bounds().Dx(); j++ {
            index := i*incanvas.Stride + j*4
            R := incanvas.Pix[index]
            G := incanvas.Pix[index+1]
            B := incanvas.Pix[index+2]
            col, _ := ConvertColor(R,G,B, colormode, cols)
            if (addinfo) {
                cc[col]++
            }
            buffer.WriteString(string("\033[48;5;") + strconv.Itoa(col) + string(";m"))
            buffer.WriteRune(32)
        }
        buffer.WriteString(string("\033[0m\n"))
    }
    if addinfo {
        buffer.WriteString(GenerateAddInfo(&cc, colornum, true))
    }
}

func UniConvert (incanvas *image.RGBA, buffer *bytes.Buffer, colormode uint8, addinfo bool, colornum int) {
    cols := InitCols()
    cc := make(map[int]int)
    for i := 0; i < incanvas.Bounds().Dy() - 2; i+=2 {
        for j := 0; j < incanvas.Bounds().Dx(); j++ {
            index := i*incanvas.Stride + j*4
            R := incanvas.Pix[index]
            G := incanvas.Pix[index+1]
            B := incanvas.Pix[index+2]
            R1 := incanvas.Pix[index + incanvas.Stride]
            G1 := incanvas.Pix[index + incanvas.Stride + 1]
            B1 := incanvas.Pix[index + incanvas.Stride + 2]
            col, _ := ConvertColor(R,G,B, colormode, cols)
            col1, _ := ConvertColor(R1,G1,B1, colormode, cols)
            if (addinfo) {
                cc[col]++
            }
            buffer.WriteString(string("\033[38;5;") + strconv.Itoa(col) + string(";48;5;") + strconv.Itoa(col1) + string("m"))
            buffer.WriteRune('▀')
        }
        buffer.WriteString(string("\033[0m\n"))
    }
    if addinfo {
        buffer.WriteString(GenerateAddInfo(&cc, colornum, true))
    }
}


func AsciiConvert (incanvas *image.RGBA, buffer *bytes.Buffer, runepalette []rune, addinfo bool, colornum int) {
    palettesize := len(runepalette)
    cc := make(map[int]int)
    for i := 0; i < incanvas.Bounds().Dy(); i++ {
        for j := 0; j < incanvas.Bounds().Dx(); j++ {
            index := i*incanvas.Stride + j*4
            R := incanvas.Pix[index]
            G := incanvas.Pix[index+1]
            B := incanvas.Pix[index+2]
            L := (int)((0.21*float32(R))+(0.72*float32(G))+(0.07*float32(B)))
            char := runepalette[ClampInt(L*palettesize/255, 0, palettesize-1)]
            if addinfo {
                cc[int(char)]++
            }
            buffer.WriteRune(char)
        }
        buffer.WriteRune('\n')
    }
    if addinfo {
        buffer.WriteString(GenerateAddInfo(&cc, colornum, false))
    }
}

func GenerateRuneBuffer (inimage *image.RGBA, palette string, ansimode bool, halfblock bool, colormode uint8, addinfo bool) (*bytes.Buffer) {
    buf := new(bytes.Buffer)
    if (ansimode && halfblock) {
        UniConvert(inimage, buf, colormode, addinfo, 16)

    } else if (ansimode) {
        AnsiConvert(inimage, buf, colormode, addinfo, 16)

    } else {
        RunePalette := []rune(palette)
        AsciiConvert(inimage, buf, RunePalette, addinfo, 16)
    }
    return buf
}

func GenerateWritableRGBA (img image.Image) (*image.RGBA) {
    originalcanvas := image.NewRGBA(image.Rect(0, 0, img.Bounds().Dx(), img.Bounds().Dy()))
    draw.Draw(originalcanvas, originalcanvas.Bounds(), img, img.Bounds().Min, draw.Src)
    return originalcanvas
}

func ImageResize (img image.Image, tx int, ty int, cpunum int, preserveratio bool, bilinear bool, FontHeight float64, halfblock bool, ansimode bool) (*image.RGBA) {
    nx, ny := CalculateAspectRatio(img, tx, ty, preserveratio, FontHeight, halfblock, ansimode)
    originalcanvas := GenerateWritableRGBA(img)
    targetcanvas := image.NewRGBA(image.Rect(0, 0, nx, ny))
    wg := new(sync.WaitGroup)
    for i := 0; i < cpunum; i++ {
        wg.Add(1)
        if bilinear {
            go Bilinear(originalcanvas, targetcanvas, wg, i, cpunum)
        } else {
            go NearestNeighbor(originalcanvas, targetcanvas, wg, i, cpunum)
        }
    }
    wg.Wait()
    return targetcanvas
}

func GetTermSize () (winsize) {
    if runtime.GOOS == "linux" || runtime.GOOS == "android" {
        ws := &winsize{}
        syscall.Syscall(syscall.SYS_IOCTL,
        uintptr(syscall.Stdin),
        uintptr(syscall.TIOCGWINSZ),
        uintptr(unsafe.Pointer(ws)))
        return *ws
    } else {
        fmt.Println("Automatic size detection is not supported on " + runtime.GOOS + ".")
        return winsize{85, 40, 0, 0}
    }
}

func ReverseUnicode(s string) string {
    runes, reverserunes := []rune(s), []rune(s)
    for i := 0; i < len(runes); i++ {
        reverserunes[(len(runes)-1)-i] = runes[i]
    }
    return string(reverserunes)
}

func WriteConfig (path string, conf *Conf) error {
    file, err := os.Create(path)
    Encoder := json.NewEncoder(file)
    Encoder.Encode(&conf)
    file.Sync()
    file.Close()
    return err
}

func ReadConfig (path string) (*Conf, error) {
    conf := new(Conf)
    file, err := os.Open(path)
    Decoder := json.NewDecoder(file)
    Decoder.Decode(&conf)
    file.Close()
    return conf, err
}

func InitializeConf () (*Conf) {
    conf := new(Conf)
    conf.Palettes = make(map[string]string)
    conf.Palettes["ascii"] = " .,:;1f50$&#NM"
    conf.Palettes["blocks"] = " ▁▂▃▄▅▆▇"
    conf.AspectRatio = false
    conf.Cores = runtime.NumCPU()
    conf.ReversePalette = false
    conf.AnsiColors = false
    conf.FontHeight = 0.5
    conf.Bilinear = false
    conf.RenderTime = false
    conf.ColorMode = "X11"
    conf.AddInfo = false
    conf.Dithering = false
    conf.DitheringMatrix = "FloydSteinberg"
    conf.HalfBlock = false
    return conf
}

func InitCols () (*[16]rgb) {
    var table [16]rgb
    table[0] = rgb{0,0,0}
    table[1] = rgb{128,0,0}
    table[2] = rgb{0,128,0}
    table[3] = rgb{128,128,0}
    table[4] = rgb{0,0,128}
    table[5] = rgb{128,0,128}
    table[6] = rgb{0,128,128}
    table[7] = rgb{192,192,192}
    table[8] = rgb{128,128,128}
    table[9] = rgb{255,0,0}
    table[10] = rgb{0,255,0}
    table[11] = rgb{255,255,0}
    table[12] = rgb{0,0,255}
    table[13] = rgb{255,0,255}
    table[14] = rgb{0,255,255}
    table[15] = rgb{255,255,255}
    return &table
}

func BurkesMatrix () *[]DitherData {
    arr := []DitherData{
    DitherData{1, 0, 5, 32},
    DitherData{2, 0, 3, 32},
    DitherData{-2, 1, 2, 32},
    DitherData{-1, 1, 4, 32},
    DitherData{0, 1, 5, 32},
    DitherData{1, 1, 4, 32},
    DitherData{2, 1, 2, 32},
    DitherData{1, 2, 2, 32},
    DitherData{1, 2, 3, 32},
    DitherData{1, 2, 2, 32},
    }
    return &arr
}

func SierraMatrix () *[]DitherData {
    arr := []DitherData{
    DitherData{1, 0, 5, 32},
    DitherData{2, 0, 3, 32},
    DitherData{-2, 1, 2, 32},
    DitherData{-1, 1, 4, 32},
    DitherData{0, 1, 5, 32},
    DitherData{1, 1, 4, 32},
    DitherData{2, 1, 2, 32},
    DitherData{1, 2, 2, 32},
    DitherData{1, 2, 3, 32},
    DitherData{1, 2, 2, 32},
    }
    return &arr
}

func FloydSteinbergMatrix () *[]DitherData {
    arr := []DitherData{
    DitherData{1, 0, 7, 16},
    DitherData{-1, 1, 3, 16},
    DitherData{0, 1, 5, 16},
    DitherData{1, 1, 1, 16},
    }
    return &arr
}


func WriteGzip (buf *bytes.Buffer, path string) error {
    file, err := os.Create(path + ".gz")
    Writer := gzip.NewWriter(file)
    Writer.Write(buf.Bytes())
    Writer.Flush()
    file.Close()
    return err
}

func ReadGzip (path string) (*bytes.Buffer, error) {
    file, _ := os.Open(path)
    defer file.Close()
    buf := new(bytes.Buffer)
    Reader, err := gzip.NewReader(file)
    if err == nil {
        io.Copy(buf, Reader)
        Reader.Close()
    }
    return buf, err
}

func CompareConfigs (def *Conf, orig *Conf, mod *Conf) *Conf {
    rdef := reflect.ValueOf(def).Elem()
    rorig := reflect.ValueOf(orig).Elem()
    rmod := reflect.ValueOf(mod).Elem()
    for i := 0; i < rdef.NumField(); i++ {
        if rorig.Type().Field(i).Name != "Palettes" && rmod.Field(i).Interface() != rdef.Field(i).Interface() {
            rorig.Field(i).Set(rmod.Field(i))
        }
    }
    TempConf := rorig.Interface().(Conf)
    return &TempConf
}

func main(){

    Terminal := GetTermSize()

    ConfPath := flag.String("s", os.Getenv("HOME")+"/.toy.json", "Custon config file path to use (for both saving and normal usage)")
    DefConf := InitializeConf()
    FlagState := InitializeConf()
    Config, cerr := ReadConfig(*ConfPath)
    if cerr != nil {
        Config = DefConf
    }

    InX := flag.Int("x", int(Terminal.Col), "Column dimensions")
    InY := flag.Int("y", int(Terminal.Row), "Row dimensions")
    Gzip := flag.Bool("g", false, "Instead of rendering, output a gzip-compressed file")
    WriteConf := flag.Bool("o", false, "Write current configuration to ~/.toy.json")
    SavePalette := flag.String("k", "", "Save a palette specified with -p")
    DeletePalette := flag.String("d", "", "Delete a palette from config")
    Palette := flag.String("p", DefConf.Palettes["ascii"], "Symbol palette to use")

    flag.IntVar(&FlagState.Cores, "j", DefConf.Cores, "Number of CPU cores to use")
    flag.BoolVar(&FlagState.AspectRatio, "a", DefConf.AspectRatio, "Disable preservation of the aspect ratio")
    flag.BoolVar(&FlagState.ReversePalette, "r", DefConf.ReversePalette, "Reverse palette")
    flag.BoolVar(&FlagState.AnsiColors, "c", DefConf.AnsiColors, "Enable rendering with ANSI colors")
    flag.BoolVar(&FlagState.RenderTime, "t", DefConf.RenderTime, "Output rendering time")
    flag.Float64Var(&FlagState.FontHeight, "f", DefConf.FontHeight, "Adjust vertical resolution based on font height")
    flag.BoolVar(&FlagState.Bilinear, "b", DefConf.Bilinear, "Use bilinear interpolation sampling for image resizing")
    flag.StringVar(&FlagState.ColorMode, "m", DefConf.ColorMode, "Color mode to use when rendering in ansi mode. Available - X11 (256 colors), X11GS (Grayscale 256), base16 (Base 16 colors)")
    flag.BoolVar(&FlagState.AddInfo, "i", DefConf.AddInfo, "Print additional image information")
    flag.StringVar(&FlagState.DitheringMatrix, "l", DefConf.DitheringMatrix, "Dithering matrix to use. Available: FloydSteinberg, Burkes, Sierra")
    flag.BoolVar(&FlagState.Dithering, "u", DefConf.Dithering, "Use dithering")
    flag.BoolVar(&FlagState.HalfBlock, "h", DefConf.HalfBlock, "Use unicode half-blocks for better resolution")

    flag.Parse()

    Config = CompareConfigs(DefConf, Config, FlagState)
    Config.AspectRatio = true

    var cmode uint8
    switch Config.ColorMode {
        case "X11":
            cmode = 0
        case "base16":
            cmode = 1
        case "X11GS":
            cmode = 2
        default:
            cmode = 0
    }

    var dm *[]DitherData
    switch Config.DitheringMatrix {
        case "FloydSteinberg":
            dm = FloydSteinbergMatrix()
        case "Sierra":
            dm = SierraMatrix()
        case "Burkes":
            dm = BurkesMatrix()
        default:
            dm = FloydSteinbergMatrix()
    }

    if (flag.NFlag() == 0) && (flag.NArg() == 0) {
        flag.Usage()
        os.Exit(0)
    }

    for k,v := range Config.Palettes {
        if k == *Palette {
            *Palette = v
            break
        }
    }

    if Config.ReversePalette {
        *Palette = ReverseUnicode(*Palette)
    }

    if len(*SavePalette) > 0 {
        if len(*Palette) > 0 {
            TempConf, perr := ReadConfig(*ConfPath)
            if perr != nil {
                 TempConf = InitializeConf()
            }
            TempConf.Palettes[*SavePalette] = *Palette
            fmt.Println("Saving current palette to " + *ConfPath + "...")
            WriteConfig(*ConfPath, TempConf)
        } else {
            fmt.Println("Please specify a palette with -p!")
        }
    }

    if len(*DeletePalette) > 0 {
        TempConf, _ := ReadConfig(*ConfPath)
        if _, ok := TempConf.Palettes[*DeletePalette]; ok {
            delete(TempConf.Palettes, *DeletePalette)
            WriteConfig(*ConfPath, TempConf)
        }
    }

    for _, FilePath := range flag.Args() {

        StartTime := time.Now()

        file, err := os.Open(FilePath)
        defer file.Close()

        if (FilePath[0] == '-') && (err != nil) {
            fmt.Println("Ignoring a flag specified after the filename")
            continue
        } else if (err != nil) {
            fmt.Println("Unable to open file")
            continue
        }

        var buf *bytes.Buffer
        var end *image.RGBA
        img, _, derr := image.Decode(file)

        if (derr == nil) {
            end = ImageResize(img, *InX, *InY, Config.Cores, Config.AspectRatio, Config.Bilinear, Config.FontHeight, Config.HalfBlock, Config.AnsiColors)
            if (Config.Dithering) {
                Dither(end, cmode, dm)
            }
            buf = GenerateRuneBuffer(end, *Palette, Config.AnsiColors, Config.HalfBlock, cmode, Config.AddInfo)
        } else {
            var gerr error
            buf, gerr = ReadGzip(FilePath)
            if (gerr != nil) {
                fmt.Println("Unable to decode a file")
                continue
            }
        }

        EndTime := time.Since(StartTime)

        if *Gzip {
            fmt.Println("Writing" + FilePath + ".gz...")
            ferr := WriteGzip(buf, FilePath)
            if ferr != nil {
                fmt.Println("Unable to write the file")
                continue
            }
        } else {
            fmt.Println(string(bytes.Runes(buf.Bytes())))
        }

        if (Config.RenderTime) {
            fmt.Println("Time spent:", EndTime)
        }
    }

    if (*WriteConf) {
        FlagState.Palettes = Config.Palettes
        fmt.Println("Writing configuration file to " + *ConfPath)
        err := WriteConfig(*ConfPath, FlagState)
        if err != nil {
            fmt.Println("Error writing configuration file")
        }
    }

}
